class Account {
    constructor(balance, currenty, number) {
        this.balance = balance;
        this.currenty =currenty;
        this.number = number;
    }
}

class Person {
    constructor(firstName, lastName, accountList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountList = accountList;
    }
    _calculateBalance() {
        let sum = 0;
        for (let account of this.accountList) {
            sum = account.balance + sum;
        }
        return sum;
    }
    addAccount(addBalance, addCurrenty, addNumber) {
        return this.accountList.push(new Account(addBalance, addCurrenty, addNumber))
    }
    sayHello() {
        return `First name: ${this.firstName}, Last name: ${this.lastName},
         Amount of acc: ${this.accountList.length}, Total balance: ${this._calculateBalance()}`
    }
    filterPositiveAccounts() {
        return this.accountList.filter((elem) => {
            return elem.balance > 0
        });
    }
    findAccount(accountNumber) {
        var t = 0;
        for(var i = 0; i < 1000000000; i++) {
            t = t + i;
        }
        return this.accountList.find((account) => accountNumber === account.number);
    }
    withdraw(amount, accountNumber) {
        
        return new Promise((resolve, reject) => {
            const foundAcc = this.findAccount(accountNumber);
            if (foundAcc && foundAcc.balance >= amount) {
                const countedMoney = foundAcc.balance - amount;
                setTimeout(() => resolve(`Account number: ${foundAcc.number}, Withdrawn: ${amount}, 
                    New balance: ${foundAcc.balance = countedMoney} `), 3000);
            }
            else if (!foundAcc) {
                reject('This account not exist');
            }
            else {
                reject('You dont have money on account');
            }
        });
    }
}
 const piwnicki = new Person ('Jan','Walec',[new Account(5340,'zloty',1),new Account(85758,'EURO',2)]);
 console.log(piwnicki.sayHello());
 console.log(piwnicki.filterPositiveAccounts());
 console.log (piwnicki.addAccount(4444,'jen',3));
 console.log(piwnicki.accountList);
 //piwnicki.withdraw(4244,2)
 //.then ((success)=>console.log(success))
 //.catch ((err)=>console.log(err));
 Promise.resolve().then(() => piwnicki.findAccount(3)).then(x => console.log(x));
 console.log("dzien dobry!");

