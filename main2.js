var person = (function () {
    let details = {
        firstName: 'Jan',
        lastName: 'Nowak ',
    };
    function calculateBalance() {
        let sum = 0;
        for (let i = 0; i < person.accountList.length; i++) {
            sum = person.accountList[i].balance + sum;
        }
        return sum;
    }
    return {
        accountList: [
            {
                balance: 73773,
                currenty: 'zloty'
            },
           {
                balance: 83839,
                currenty: 'euro'
            }
        ],
        firstName: details.firstName,
        lastName: details.lastName,
        sayHello: function (){
            return 'Name:' + this.firstName + ', Surname: ' + this.lastName + ', Sum Balance: ' + calculateBalance()
                + ', Number of accounts: ' + person.accountList.length;
        },
        addAccount: function (balans, waluta) {
            this.accountList.push({ balance: balans, waluta: waluta })
        }
    };
}
)();
console.log(person.sayHello());
person.addAccount(87438743, 'jen');
console.log(person.sayHello());

