var Account = function (balance,currenty) {
    this.balance=balance;
    this.currenty=currenty;
};

var person = (function () {
    let details = {
        firstName: 'Jan',
        lastName: 'Nowak '
    };
    function calculateBalance() {
        var sum = 0;
        for (var i = 0; i < person.accountList.length; i++) {
            sum = person.accountList[i].balance + sum;
        }
        return sum;
    }
    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountList: [new Account(14400,"geld"), new Account(41338, "green")],
        sayHello: function (){
            return 'Name:' + this.firstName + ', Surname: ' + this.lastName + ', Sum Balance: ' + calculateBalance()
                + ', Number of accounts: ' + this.accountList.length;
        },
        addAccount: function (account) {
            this.accountList.push(account);
        }
    }
}
)();
console.log(person.sayHello());
person.addAccount(new Account (87443, 'jen'));
console.log(person.sayHello());


