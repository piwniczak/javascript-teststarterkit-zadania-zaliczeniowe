var ctrl = (() => {
    class Account {
        constructor(number, balance, currency) {
            this.number = number;
            this.balance = balance;
            this.currency = currency;
        }
    }

    class Person {
        constructor(firstName, lastName, accountList) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.accountList = accountList;
        }
        addAccount(addNumber, addBalance, addCurrency) {
            return this.accountList.push(new Account(addNumber, addBalance, addCurrency))
        }

        findAccount(accountNumber) {
            return this.accountList.find((account) => { return account.number == accountNumber });
        }
        withdraw(accountNumber, amount) {
            let account = this.findAccount(accountNumber);
            return new Promise((resolve, reject) => {
                if (account && amount <= account.balance) {
                    setTimeout(() => {
                        account.balance -= amount;
                        resolve({
                            message: `Account nr: ${accountNumber} - new balance: ${account.balance} ${account.currency}, Withdrawn: ${amount} ${account.currency}`,
                            balance: account.balance
                        });
                    }, 3000);
                } else if (!account || amount > account.balance) {
                    reject(`Account does exist ${accountNumber} or balance smaller than  withdrawal`);
                }
            })
        }
    }
    const piwnicki = new Person('Jan', 'Walec', [new Account(1, 3333, 'PLN'), new Account(2, 4444, 'EURO')]);
    piwnicki.addAccount(3, 4444, 'THB');

    document.addEventListener("DOMContentLoaded", function (event) {
        let personNameBox = document.querySelector(".card-title");
        let personAccBox = document.querySelector(".card-text");
        let numberInput = document.querySelector("#number");
        let amountInput = document.querySelector("#amount");
        let withdrawButton = document.querySelector(".btn.btn-primary");
        let messageBox = document.querySelector("#message");

        withdrawButton.setAttribute('disabled', 'true');

        function inputChecker() {
            let accountNumber = numberInput.value;
            let amount = amountInput.value;

            withdrawButton.disabled =!(accountNumber && amount);
        }

         withdrawal = function(event, personData) {
            let accountNumber = parseFloat(numberInput.value);
            let amount = parseFloat(amountInput.value);

            personData.withdraw(accountNumber, amount)
                .then((success) => {
                    console.log(success.message);
                    messageBox.innerText = success.message;
                    const accountSelector = document.querySelector("#acc_" + accountNumber);
                    const foundAccount = personData.findAccount(accountNumber);
                    accountSelector.innerText = `Account nr: ${foundAccount.number}, Amount: ${foundAccount.balance}, Currency: ${foundAccount.currency}`
                })
                .catch((failure) => {
                    console.log(failure);
                    messageBox.innerText = failure;
                });
        }

        function createPersonCard(personData) {
            personNameBox.innerText = ` ${personData.firstName} ${personData.lastName}`;
            for (let i = 0; i < personData.accountList.length; i++) {
                let number = personData.accountList[i].number;
                let newParagraf = document.createElement("p");
                newParagraf.id = "acc_" + number;
                let newParagrafText = document.createTextNode(`Account nr: ${personData.accountList[i].number}, Amount: ${personData.accountList[i].balance}, Currency: ${personData.accountList[i].currency}`);
                newParagraf.appendChild(newParagrafText);
                personAccBox.appendChild(newParagraf);
            }
        }

        withdrawButton.addEventListener("click", (event) => withdrawal(event, piwnicki));
        numberInput.addEventListener("change", inputChecker);
        amountInput.addEventListener("change", inputChecker);
        numberInput.addEventListener("keyup", inputChecker);
        amountInput.addEventListener("keyup", inputChecker);

        createPersonCard(piwnicki);
    });

})();
